using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GymTracker
{
    class Global
    {
        public static string DatabasePath
        {
            get
            {
                return FileAccessHelper.GetLocalFilePath("gym_tracker.db");
            }
        }


    }
}