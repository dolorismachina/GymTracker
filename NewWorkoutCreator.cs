using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GymTracker
{
    class NewWorkoutCreator
    {
        public static void ShowDialog(Activity context, EventHandler<NewWorkoutConfirmedArgs> confirmCallback)
        {
            NewWorkoutDialog dialog = new NewWorkoutDialog();
            var transaction = context.FragmentManager.BeginTransaction();
            dialog.Show(transaction, "NewWorkoutDialog");
            dialog.NewWorkoutConfirmed += confirmCallback;
        }


        /// <summary>
        /// Helper method to create new workout. It adds a new row in the Workout table
        /// and starts tha Workout activity.
        /// </summary>
        /// <param name="context">Activity that starts the Workout activity.</param>
        /// <param name="args">Arguments received from the NewWorkoutDialog.</param>
        public static void CreateNewWorkout(Activity context, NewWorkoutConfirmedArgs args)
        {
            var tracker = new GymTracker();
            string date = string.Format("{0}-{1}-{2}", args.Year, args.Month, args.Day);

            tracker.InsertWorkout(date, args.Name);
            var lastId = tracker.GetLastId("Workout");

            Intent i = new Intent(context, typeof(WorkoutActivity));
            i.PutExtra(GymTracker.WORKOUT_ID, lastId);
            context.StartActivity(i);
        }
    }
}