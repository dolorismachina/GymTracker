using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;

namespace GymTracker
{
    class NewWorkoutDialog : DialogFragment
    {
        public event EventHandler<NewWorkoutConfirmedArgs> NewWorkoutConfirmed;

        private Button okButton;
        private NumberPicker dayPicker;
        private NumberPicker monthPicker;
        private NumberPicker yearPicker;
        private EditText workoutName;

        private string[] days;
        private string[] months;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            days = new string[31];
            for (var i = 1; i <= 31; i++)
            {
                days[i - 1] = i.ToString("00");
            }

            months = new string[12];
            for (var i = 1; i <= 12; i++)
            {
                months[i - 1] = i.ToString("00");
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var v = inflater.Inflate(Resource.Layout.NewWorkoutDialog, container, false);

            dayPicker = v.FindViewById<NumberPicker>(Resource.Id.dayPicker);
            monthPicker = v.FindViewById<NumberPicker>(Resource.Id.monthPicker);
            yearPicker = v.FindViewById<NumberPicker>(Resource.Id.yearPicker);

            workoutName = v.FindViewById<EditText>(Resource.Id.workoutName);

            okButton = v.FindViewById<Button>(Resource.Id.confirmNewWorkout);
            okButton.Click += NewWorkoutConfirmClick;

            AssignValuesToPickers();

            return v;
        }

        private void NewWorkoutConfirmClick(object sender, EventArgs e)
        {
            if (NewWorkoutConfirmed != null)
            {
                NewWorkoutConfirmedArgs args = 
                    new NewWorkoutConfirmedArgs(
                        days[dayPicker.Value], 
                        months[monthPicker.Value], 
                        yearPicker.Value.ToString(), workoutName.Text);

                NewWorkoutConfirmed(this, args);
                Dismiss();
            }
        }

        private void AssignValuesToPickers()
        {
            DateTime date = DateTime.Today;

            dayPicker.SetDisplayedValues(days);
            dayPicker.MinValue = 0;
            dayPicker.MaxValue = 30;
            dayPicker.WrapSelectorWheel = false;
            dayPicker.Value = date.Day - 1;

            monthPicker.SetDisplayedValues(months);
            monthPicker.MinValue = 0;
            monthPicker.MaxValue = 11;
            monthPicker.WrapSelectorWheel = false;
            monthPicker.Value = date.Month - 1;

            yearPicker.MinValue = 2000;
            yearPicker.MaxValue = 2020;
            yearPicker.WrapSelectorWheel = false;
            yearPicker.Value = date.Year;
        }
    }

    public class NewWorkoutConfirmedArgs : EventArgs
    {
        public string Day { get; }
        public string Month { get; }
        public string Year { get; }
        public string Name { get; }

        public NewWorkoutConfirmedArgs(string day, string month, string year, string name = "")
        {
            Day = day;
            Month = month;
            Year = year;
            Name = name;
        }
    }
}