using SQLite;

namespace GymTracker.Models
{
    [Table("Workout")]
    public class WorkoutModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [NotNull]
        public string Date { get; set; }

        [NotNull]
        public string Name { get; set; }

        public WorkoutModel() { }

        public WorkoutModel(int id, string date, string name = "")
        {
            this.Id = id;
            this.Date = date;
            this.Name = name;
        }

        public WorkoutModel(string date, string name = "")
        {
            this.Date = date;
            this.Name = name;
        }

        public override string ToString()
        {
            return string.Format("Workout: {0} Date: {1} Name {2}", Id, Date, Name);
        }
    }
}