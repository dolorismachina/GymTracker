using SQLite;

namespace GymTracker.Models
{
    [Table("ExerciseSet")]
    public class ExerciseSet
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        
        [NotNull]
        public float Weight { get; set; }

        [NotNull]
        public int Repetitions { get; set; }

        [NotNull]
        public int WorkoutId { get; set; }

        [NotNull]
        public int ExerciseId { get; set; }

        public ExerciseSet() { }

        public ExerciseSet(int id, float weight, int repetitions, int workoutId, int exerciseId)
        {
            this.Id = id;
            this.Weight = weight;
            this.Repetitions = repetitions;
            this.WorkoutId = workoutId;
            this.ExerciseId = exerciseId;
        }

        public ExerciseSet(float weight, int repetitions, int workoutId, int exerciseId)
        {
            this.Weight = weight;
            this.Repetitions = repetitions;
            this.WorkoutId = workoutId;
            this.ExerciseId = exerciseId;
        }
    }
}