using SQLite;

namespace GymTracker.Models
{
    [Table("Exercise")]
    public class ExerciseModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [NotNull, Unique]
        public string Name { get; set; }

        public ExerciseModel() { }

        public ExerciseModel(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public ExerciseModel(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            return string.Format("Exercise {0}:) {1}", Id, Name);
        }
    }
}