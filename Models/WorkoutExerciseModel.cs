using SQLite;

namespace GymTracker.Models
{
    [Table("WorkoutExercise")]
    class WorkoutExerciseModel
    {
        [NotNull]
        public int WorkoutId { get; set; }
        
        [NotNull]
        public int ExerciseId { get; set; }

        public WorkoutExerciseModel() { }

        public WorkoutExerciseModel(int workoutId, int exerciseId)
        {
            WorkoutId = workoutId;
            ExerciseId = exerciseId;
        }
    }
}