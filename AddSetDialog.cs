using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GymTracker
{
    public class AddSetDialog : DialogFragment
    {
        public event EventHandler<ConfirmClickEventArgs> ConfirmClicked;

        NumberPicker weightPicker;
        NumberPicker repetitionsPicker;
        Button confirmButton;

        private int defaultWeight;
        private int defaultRepetitions;

        public AddSetDialog() { }
        public AddSetDialog(int weight, int repetitions)
        {
            defaultWeight = weight;
            defaultRepetitions = repetitions;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var v = inflater.Inflate(Resource.Layout.AddSetDialog, container, false);
            weightPicker = v.FindViewById<NumberPicker>(Resource.Id.weightPicker);
            repetitionsPicker = v.FindViewById<NumberPicker>(Resource.Id.repetitionsPicker);
            weightPicker.MinValue = 0;
            weightPicker.MaxValue = 300;

            repetitionsPicker.MinValue = 0;
            repetitionsPicker.MaxValue = 50;

            weightPicker.Value = defaultWeight;
            repetitionsPicker.Value = defaultRepetitions;

            confirmButton = v.FindViewById<Button>(Resource.Id.confirmAddSet);
            confirmButton.Click += (sender, args) => OnConfirmClicked();

            return v;
        }

        private void OnConfirmClicked()
        {
            if (ConfirmClicked != null)
            {
                ConfirmClickEventArgs args = new ConfirmClickEventArgs(weightPicker.Value, repetitionsPicker.Value);
                Dismiss();
                ConfirmClicked(this, args);
            }
        }
    }

    public class ConfirmClickEventArgs : EventArgs
    {
        public ConfirmClickEventArgs(int weight, int repetitions)
        {
            Weight = weight;
            Repetitions = repetitions;
        }

        public int Weight { get; set; }
        public int Repetitions { get; set; }
    }
}