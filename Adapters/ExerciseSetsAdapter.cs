using Android.App;
using Android.Views;
using Android.Widget;
using GymTracker.Models;
using System.Collections.Generic;

namespace GymTracker
{
    public class ExerciseSetsAdapter : BaseAdapter<ExerciseSet>
    {
        IList<ExerciseSet> sets;
        Activity context;

        public ExerciseSetsAdapter(Activity context, IList<ExerciseSet> items)
            : base()
        {
            this.context = context;
            this.sets = items;
        }

        public override int Count
        {
            get
            {
                return sets.Count;
            }
        }

        public override ExerciseSet this[int position]
        {
            get
            {
                return sets[position];
            }
        }

        public override long GetItemId(int position)
        {
            return sets[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.CustomSetItem, null);
            }

            var weight = sets[position].Weight.ToString();
            var repetitions = sets[position].Repetitions.ToString();

            view.FindViewById<TextView>(Resource.Id.setWeight).Text = string.Format("You lifted {0} kg", weight);
            view.FindViewById<TextView>(Resource.Id.setRepetitions).Text = string.Format("{0} times", repetitions);

            return view;
        }
    }
}