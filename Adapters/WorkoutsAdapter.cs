using Android.App;
using Android.Views;
using Android.Widget;
using GymTracker.Models;
using System.Collections.Generic;

namespace GymTracker
{
    public class WorkoutsAdapter : BaseAdapter<Models.WorkoutModel>
    {
        IList<Models.WorkoutModel> workouts;
        Activity context;

        public WorkoutsAdapter(Activity context, IList<Models.WorkoutModel> items)
            : base()
        {
            this.context = context;
            this.workouts = items;
        }

        public override int Count
        {
            get
            {
                return workouts.Count;
            }
        }

        public override WorkoutModel this[int position]
        {
            get
            {
                return workouts[position];
            }
        }

        public override long GetItemId(int position)
        {
            return workouts[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.CustomWorkoutItem, null);
            }

            view.FindViewById<TextView>(Resource.Id.workoutDate).Text = workouts[position].Date;
            view.FindViewById<TextView>(Resource.Id.workoutName).Text = workouts[position].Name;

            return view;
        }
    }
}