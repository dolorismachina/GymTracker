using Android.App;
using Android.Views;
using Android.Widget;
using GymTracker.Models;
using System.Collections.Generic;

namespace GymTracker
{
    public class ExercisesAdapter : BaseAdapter<Models.ExerciseModel>
    {
        IList<Models.ExerciseModel> exercises;
        Activity context;

        public ExercisesAdapter(Activity context, IList<Models.ExerciseModel> items)
            : base()
        {
            this.context = context;
            this.exercises = items;
        }

        public override int Count
        {
            get
            {
                return exercises.Count;
            }
        }

        public override ExerciseModel this[int position]
        {
            get
            {
                return exercises[position];
            }
        }

        public override long GetItemId(int position)
        {
            return exercises[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.CustomExerciseItem, null);
            }

            view.FindViewById<TextView>(Resource.Id.exerciseName).Text = exercises[position].Name;

            return view;
        }
    }
}