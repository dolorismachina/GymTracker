using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using SQLite;
using GymTracker.Models;

namespace GymTracker
{
    class GymTracker
    {
        public const string WORKOUT_ID = "WorkoutId";
        public const string EXERCISE_ID = "ExerciseId";

        private string databaseName;
        private string databaseLocation;
        private string databasePath;

        public SQLiteConnection Connection { get; private set; }

        public GymTracker()
        {
            EstablishConnection();
        }

        public void SetupDatabase()
        {
            CreateTables();
        }

        private void InsertMockExercises()
        {
            Connection.DeleteAll<ExerciseModel>();

            List<int> ids = new List<int>();
            ids.Add(InsertExercise("Squat"));
            ids.Add(InsertExercise("Deadlift"));
            ids.Add(InsertExercise("Bench Press"));
        }

        private void InsertMockWorkouts()
        {
            Connection.DeleteAll<WorkoutModel>();

            List<int> ids = new List<int>();
            ids.Add(InsertWorkout("2016-04-01", "Chest Day"));
            ids.Add(InsertWorkout("2016-04-03", "Leg Day"));
        }

        private void InsertMockSets()
        {
            Connection.DeleteAll<ExerciseSet>();

            InsertExerciseSet(1, 1, 5, 20);
            InsertExerciseSet(1, 1, 5, 25);
            InsertExerciseSet(1, 1, 3, 50);

            InsertExerciseSet(1, 2, 8, 20);
            InsertExerciseSet(1, 2, 8, 25);
            InsertExerciseSet(1, 2, 6, 30);

            InsertExerciseSet(1, 3, 5, 10);
            InsertExerciseSet(1, 3, 5, 20);
            InsertExerciseSet(1, 3, 5, 25);
        }

        public void InsertMockWorkoutExercises()
        {
            Connection.DeleteAll<WorkoutExerciseModel>();

            List<int> ids = new List<int>();
            InsertNewWorkoutExercise(1, 1);
            InsertNewWorkoutExercise(1, 2);
            InsertNewWorkoutExercise(1, 3);

            InsertNewWorkoutExercise(2, 1);
            InsertNewWorkoutExercise(2, 3);
        }

        private void InsertMockValues()
        {
            InsertMockExercises();
            InsertMockWorkouts();
            InsertMockWorkoutExercises();
            InsertMockSets();
        }

        public List<WorkoutModel> GetAllWorkouts()
        {
            string query = "SELECT * FROM Workout";

            return Connection.Query<WorkoutModel>(query);
        }

        public List<ExerciseModel> GetAllExercises()
        {
            string query = "SELECT * FROM Exercise";

            return Connection.Query<ExerciseModel>(query);
        }

        public List<WorkoutExerciseModel> GetAllWorkoutExercise()
        {
            var query = "SELECT * FROM WorkoutExercise";

            return Connection.Query<WorkoutExerciseModel>(query);
        }

        public List<ExerciseSet> GetAllExerciseSet()
        {
            var query = "SELECT * FROM WorkoutExercise";

            return Connection.Query<ExerciseSet>(query);
        }

        public List<ExerciseModel> GetExercisesForWorkout(int workoutId)
        {
            var query = "SELECT Exercise.Id, Exercise.Name FROM Exercise, WorkoutExercise " +
                        "WHERE WorkoutExercise.WorkoutId = ? AND Exercise.Id = WorkoutExercise.ExerciseId;";

            return Connection.Query<ExerciseModel>(query, workoutId);
        }

        public List<ExerciseSet> GetSetsForWorkoutExercise(int workoutId, int exerciseId)
        {
            var query = "SELECT * " +
                        "FROM ExerciseSet " +
                        "WHERE ExerciseSet.WorkoutId = ? " +
                        "AND ExerciseSet.ExerciseId = ?; ";

            return Connection.Query<ExerciseSet>(query, workoutId, exerciseId);
        }

        public int InsertWorkout(string date, string name = "")
        {
            WorkoutModel wk = new WorkoutModel();
            wk.Date = date;
            wk.Name = name;

            return Connection.Insert(wk);
        }

        public int InsertExercise(string name)
        {
            ExerciseModel ex = new ExerciseModel();
            ex.Name = name;

            return Connection.Insert(ex);
        }

        public int InsertNewWorkoutExercise(int workoutId, int exerciseId)
        {
            WorkoutExerciseModel wkex = new WorkoutExerciseModel();
            wkex.WorkoutId = workoutId;
            wkex.ExerciseId = exerciseId;

            return Connection.Insert(wkex);
        }

        public int InsertExerciseSet(int workoutId, int exerciseId, int repetitions, float weight)
        {
            ExerciseSet exset = new ExerciseSet();
            exset.WorkoutId = workoutId;
            exset.ExerciseId = exerciseId;
            exset.Repetitions = repetitions;
            exset.Weight = weight;

            return Connection.Insert(exset);
        }

        public int InsertExerciseSet(ExerciseSet set)
        {
            return InsertExerciseSet(set.WorkoutId, set.ExerciseId, set.Repetitions, set.Weight);
        }

        
        public int GetLastId(string tableName)
        {
            var q = "SELECT `seq` FROM `sqlite_sequence` WHERE `name` = ?";

            int result = Connection.ExecuteScalar<int>(q, tableName);

            return result;
        }

        public void CreateTables()
        {
            var createExerciseTableQuery =
                "CREATE TABLE IF NOT EXISTS `Exercise` ( " +
                "`Id`	INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "`Name`	TEXT NOT NULL UNIQUE);";

            var createWorkoutTableQuery =
                "CREATE TABLE IF NOT EXISTS `Workout` ( " +
                "`Id`	INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "`Date`	TEXT NOT NULL, " +
                "`Name`	TEXT);";

            var createWorkoutExerciseTableQuery =
                "CREATE TABLE IF NOT EXISTS `WorkoutExercise` ( " +
                "`WorkoutId`    INTEGER NOT NULL, " +
                "`ExerciseId`   INTEGER NOT NULL, " +

                "PRIMARY KEY(WorkoutId, ExerciseId), " +
                "FOREIGN KEY(`WorkoutId`)   REFERENCES `Workout`(`Id`)  ON DELETE CASCADE ON UPDATE CASCADE, " +
                "FOREIGN KEY(`ExerciseId`)  REFERENCES `Exercise`(`Id`) ON DELETE CASCADE ON UPDATE CASCADE);";

            var createExerciseSetTableQuery =
                "CREATE TABLE IF NOT EXISTS `ExerciseSet` ( " +
                "`Id`	        INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "`Weight`	    REAL NOT NULL, " +
                "`Repetitions`	INTEGER NOT NULL, " +
                "`WorkoutId`	INTEGER NOT NULL, " +
                "`ExerciseId`	INTEGER NOT NULL, " +

                "FOREIGN KEY(`WorkoutId`)   REFERENCES `Workout`(`Id`)  ON DELETE CASCADE ON UPDATE CASCADE, " +
                "FOREIGN KEY(`ExerciseId`)  REFERENCES `Exercise`(`Id`) ON DELETE CASCADE ON UPDATE CASCADE);";

            Connection.DropTable<ExerciseModel>();
            Connection.Execute(createExerciseTableQuery);

            Connection.DropTable<WorkoutModel>();
            Connection.Execute(createWorkoutTableQuery);

            Connection.DropTable<ExerciseSet>();
            Connection.Execute(createExerciseSetTableQuery);

            Connection.DropTable<WorkoutExerciseModel>();
            Connection.Execute(createWorkoutExerciseTableQuery);
        }

        private void ResetTables()
        {
            CreateTables();
            InsertMockValues();
        }

        private void EstablishConnection()
        {
            databaseName = "GymTracker.db";
            databaseLocation = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            databasePath = System.IO.Path.Combine(databaseLocation, databaseName);

            Connection = new SQLiteConnection(databasePath);

            EnforceForeignKeyChecking(true);

            // ResetTables();
        }

        private void EnforceForeignKeyChecking(bool enforce)
        {
            int flag;

            if (enforce)
            {
                flag = 1;
            }
            else
            {
                flag = 0;
            }

            var query = string.Format("PRAGMA foreign_keys = '{0}';", flag);
            Connection.Execute(query);
        }
    }
}