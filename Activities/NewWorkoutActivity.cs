using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;
using System.Collections.ObjectModel;

namespace GymTracker
{
    [Activity(Label = "NewWorkoutActivity")]
    public class NewWorkoutActivity : Activity
    {
        ArrayAdapter<string> listAdapter;
        ArrayAdapter<string> spinnerAdapter;

        Spinner spinner;
        Button btn_addExercise;
        ListView exercisesList;

        List<string> exerciseNames;
        List<int> exerciseIds;

        int workoutId;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.NewWorkout);

            FindViews();

            GetDataFromDatabase();

            List<string> selectedExercises = new List<string>();

            spinnerAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, exerciseNames);
            spinner.Adapter = spinnerAdapter;

            listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, selectedExercises);
            exercisesList.Adapter = listAdapter;
            exercisesList.ItemClick += ExercisesList_ItemClick;

            btn_addExercise.Click += Btn_addExercise_Click;

            AddWorkoutToDatabase();
        }

        private void ExercisesList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
        }

        private void Btn_addExercise_Click(object sender, EventArgs e)
        {
            listAdapter.Add((spinner.SelectedView as TextView).Text);
        }

        private void FindViews()
        {
            btn_addExercise = FindViewById<Button>(Resource.Id.btnAddExercise);
            exercisesList = FindViewById<ListView>(Resource.Id.workoutExercises);
            spinner = FindViewById<Spinner>(Resource.Id.spinnerExercises);
        }
        
        private void GetDataFromDatabase()
        {
            var con = new SQLiteConnection(Global.DatabasePath);
            var query = "SELECT exercise.id, exercise.name FROM exercise";
            var exercises = con.Query<Models.ExerciseModel>(query);
            exerciseNames = new List<string>();
            exerciseIds = new List<int>();
            string[] items = new string[exerciseNames.Count];
            foreach (var e in exercises)
            {
                exerciseNames.Add(e.Name);
                exerciseIds.Add(e.Id);
            }

            query = "SELECT MAX(id) FROM workout";
            string s = con.ExecuteScalar<string>(query);

            workoutId = int.Parse(s) + 1;
            con.Close();
        }

        private void AddWorkoutToDatabase()
        {
            var con = new SQLiteConnection(Global.DatabasePath);
            var query = "INSERT INTO workout(date) VALUES(?)";
            DateTime date = DateTime.Now.Date;
            var s = "2044" + "-" + date.Month + "-" + date.Day;

            var wk = new Workout();
            wk.Date = s;
            var k = con.Insert(wk);
            con.Close();

            Toast.MakeText(this, k.ToString(), ToastLength.Long).Show();
        }

        private int RetrieveWorkoutId()
        {
            return 0;
        }
    }

    [Table("workout")]
    public class Workout
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { get; set; }
        [Column("date")]
        public string Date { get; set; }
        [Column("name")]
        public string Name { get; set; }
    }
}