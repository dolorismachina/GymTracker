using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GymTracker
{
    [Activity(Label = "AllExerciseListActivity")]
    public class AllExerciseListActivity : ListActivity
    {
        string[] availableExercises;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            availableExercises = new string[]
            {
                "deadlift",
                "squat",
                "barbell press",
                "lunge",
                "pulldown"
            };

            this.ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, availableExercises);
        }
    }
}