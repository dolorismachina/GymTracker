using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.ObjectModel;

using SQLite;

namespace GymTracker
{
    [Activity(Label = "WorkoutExerciseActivity")]
    public class WorkoutExerciseActivity : Activity
    {
        ObservableCollection<string> nums;
        ListView list;
        Button btn;
        TextView exerciseName;
        TextView exerciseDate;

        LinearLayout addSetLayout;
        EditText addSetWeight;
        EditText addSetReps;

        bool addingSet = false;

        int workoutId;
        int exerciseId;

        List<Models.ExerciseSet> sets;
        GymTracker gymTracker;

        int setId;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.WorkoutExercise);

            PrepareLayout();
            gymTracker = new GymTracker();

            workoutId = Intent.GetIntExtra("WorkoutId", 0);
            exerciseId = Intent.GetIntExtra("ExerciseId", 0);

            sets = gymTracker.GetSetsForWorkoutExercise(workoutId, exerciseId);

            list.Adapter = new ExerciseSetsAdapter(this, sets);
            list.ItemClick += List_ItemClick;
        }

        private void Dialog_ConfirmClicked(object sender, ConfirmClickEventArgs e)
        {
            if (addingSet)
            {
                Models.ExerciseSet set = new Models.ExerciseSet();
                set.ExerciseId = exerciseId;
                set.WorkoutId = workoutId;
                set.Weight = e.Weight;
                set.Repetitions = e.Repetitions;

                gymTracker.InsertExerciseSet(set);
                sets = gymTracker.GetSetsForWorkoutExercise(workoutId, exerciseId);
                list.Adapter = new ExerciseSetsAdapter(this, sets);

                addingSet = false;
            }
            else
            {
                gymTracker.Connection.Execute("UPDATE `ExerciseSet` SET Weight=?, Repetitions=? WHERE Id=?;", e.Weight, e.Repetitions, setId);
                sets = gymTracker.GetSetsForWorkoutExercise(workoutId, exerciseId);
                list.Adapter = new ExerciseSetsAdapter(this, sets);
            }
        }

        private void List_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            UpdateSet((int)e.Id);
        }

        private void PrepareLayout()
        {
            list = FindViewById<ListView>(Resource.Id.exerciseSets);
            btn = FindViewById<Button>(Resource.Id.button1);
            addSetLayout = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            addSetReps = FindViewById<EditText>(Resource.Id.reps);
            addSetWeight = FindViewById<EditText>(Resource.Id.weight);
            exerciseName = FindViewById<TextView>(Resource.Id.exerciseName);
            exerciseDate = FindViewById<TextView>(Resource.Id.exerciseDate);

            btn.Click += OnButtonClick;
        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            addingSet = true;
            ShowSetInputDialog(0, 0);
        }

        private void ShowSetInputDialog(int weight, int repetitions)
        {
            AddSetDialog dialog = new AddSetDialog(weight, repetitions);
            var transaction = FragmentManager.BeginTransaction();
            dialog.Show(transaction, "dialog");
            dialog.ConfirmClicked += Dialog_ConfirmClicked;
        }

        private void UpdateSet(int setId)
        {
            this.setId = setId;

            Models.ExerciseSet selectedSet = sets.Find(x => x.Id == setId);
            ShowSetInputDialog((int)selectedSet.Weight, selectedSet.Repetitions);
        }
    }
}