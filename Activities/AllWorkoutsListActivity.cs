using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;
using GymTracker.Models;
using System.IO;

namespace GymTracker
{
    [Activity(Label = "AllWorkoutsListActivity")]
    public class AllWorkoutsListActivity : Activity
    {
        ListView list;
        Button btnNewWorkout;

        GymTracker gymTracker;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AllWorkouts);

            gymTracker = new GymTracker();

            var workouts = gymTracker.GetAllWorkouts();

            list = FindViewById<ListView>(Resource.Id.workoutsListView);
            list.Adapter = new WorkoutsAdapter(this, workouts);
            list.ItemClick += List_ItemClick;

            btnNewWorkout = FindViewById<Button>(Resource.Id.btnNewWorkout);
            btnNewWorkout.Click += BtnNewWorkout_Click;

            Toast.MakeText(this, workouts.Count.ToString(), ToastLength.Short).Show();
        }

        private void BtnNewWorkout_Click(object sender, EventArgs e)
        {
            OpenNewWorkoutDialog();
        }


        private void OpenNewWorkoutDialog()
        {
            NewWorkoutCreator.ShowDialog(this, Dialog_NewWorkoutConfirmed);
        }

        private void Dialog_NewWorkoutConfirmed(object sender, NewWorkoutConfirmedArgs e)
        {
            NewWorkoutCreator.CreateNewWorkout(this, e);
        }

        private void List_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Intent i = new Intent(this, typeof(WorkoutActivity));

            var workoutId = e.Id;
            Toast.MakeText(this, "Selected workout: " + workoutId.ToString(), ToastLength.Short).Show();
            i.PutExtra("WorkoutId", (int)workoutId);

            StartActivity(i);
        }
    }
}