﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;

using System.Collections.Generic;

using GymTracker.Models;
using Android.Views;

namespace GymTracker
{
    [Activity(Label = "GymTracker", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Button btnAllWorkouts;
        Button btnAllExercises;
        Button btnNewWorkout;

        GymTracker gymTracker;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            System.Diagnostics.Debug.WriteLine(System.Environment.SpecialFolder.MyDocuments);

            SetContentView(Resource.Layout.Main);

            PrepareLayout();

            gymTracker = new GymTracker();
        }

        private void PrepareLayout()
        {
            btnAllExercises = FindViewById<Button>(Resource.Id.btnAllExercises);
            btnAllExercises.Click += BtnAllExercises_Click;

            btnAllWorkouts = FindViewById<Button>(Resource.Id.btnAllWorkouts);
            btnAllWorkouts.Click += BtnAllWorkouts_Click;

            btnNewWorkout = FindViewById<Button>(Resource.Id.btnNewWorkout);
            btnNewWorkout.Click += NewWorkout_Click;
        }

        private void BtnAllExercises_Click(object sender, EventArgs e)
        {
            Intent i = new Intent(this, typeof(AllExerciseListActivity));
            StartActivity(i);
        }

        private void BtnAllWorkouts_Click(object sender, EventArgs e)
        {
            Intent i = new Intent(this, typeof(AllWorkoutsListActivity));
            StartActivity(i);
        }

        private void NewWorkout_Click(object sender, EventArgs e)
        {
            OpenNewWorkoutDialog();
        }

        private void OpenNewWorkoutDialog()
        {
            NewWorkoutCreator.ShowDialog(this, Dialog_NewWorkoutConfirmed);
        }

        private void Dialog_NewWorkoutConfirmed(object sender, NewWorkoutConfirmedArgs e)
        {
            Toast.MakeText(this, string.Format("Day: {0}, Month {1}, Year: {2}", e.Day, e.Month, e.Year), ToastLength.Short).Show();

            NewWorkoutCreator.CreateNewWorkout(this, e);
        }
    }
}

