using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace GymTracker
{
    [Activity(Label = "WorkoutActivity")]
    public class WorkoutActivity : Activity
    {
        ListView exerciseListView;
        TextView workoutDateTextView;

        int workoutId;

        List<Models.ExerciseModel> exercises;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Workout);
            PrepareLayout();

            workoutId = Intent.GetIntExtra(GymTracker.WORKOUT_ID, 1);

            GymTracker gymTracker = new GymTracker();

            exercises = gymTracker.GetExercisesForWorkout(workoutId);

            exerciseListView.Adapter = new ExercisesAdapter(this, exercises);

        }

        private void PrepareLayout()
        {
            workoutDateTextView = FindViewById<TextView>(Resource.Id.workoutDate);

            exerciseListView = FindViewById<ListView>(Resource.Id.workoutExerciseListView);
            exerciseListView.ItemClick += ExerciseList_ItemClick;
        }

        private void ExerciseList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Intent i = new Intent(this, typeof(WorkoutExerciseActivity));

            i.PutExtra("WorkoutId", (int)workoutId);
            i.PutExtra("ExerciseId", (int)e.Id);

            StartActivity(i);
        }
    }
}